# Google ASR client #

## Installation ##

**Download and install Google Cloud Speech**

Follow instructions in
https://cloud.google.com/sdk/downloads#apt-get

Optional installations google-cloud-sdk-app-engine-... not needed.

**Credentials** 

Obtain and install the key-file 

**Initialization** 

(see also https://cloud.google.com/sdk/docs/initializing)
```
#!bash

  $ gcloud init

```
Activate the account
Project to use: speech...
```
#!bash

  $ gcloud auth activate-service-account --key-file=<PATH_TO_KEYFILE>

```

**Install python modules**
```
#!bash

  $ sudo pip install google-api-python-client==1.5.5
  $ sudo pip install google-cloud-speech==0.22.0

```

**Add in .bashrc**

```
#!bash

export GOOGLE_APPLICATION_CREDENTIALS=<PATH_TO_KEYFILE>

```

**Install ffmpg** (needed to convert wav files in flac)

- Download from http://www.ffmpeg.org/download.html

```
#!bash

  $ wget http://ffmpeg.org/releases/ffmpeg-3.2.2.tar.bz2
```

- Extract, compile and install

```
#!bash

  $ tar xjvf ffmpeg-3.2.2.tar.bz2 
  $ cd ffmpeg-3.2.2
  $ ./configure --disable-yasm
  $ make
  $ sudo make install

```

## Run ##


```
#!bash
  $ cd transcribe
  $ python transcribe.py speech_file_folder output_file
```


# LU4R #

## Run ##

```
#!bash
java -jar lu4r-server-0.2.0.jar simple cfr en 9001
```

**Install Java 8 on Ubuntu**

```
#!bash
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
sudo update-alternatives --config java
```

# Streaming (ASR from microphone) #

https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/speech/grpc/transcribe_streaming.py

Requirements:
```
#!bash
grpcio==1.0.4
PyAudio==0.2.10
grpc-google-cloud-speech-v1beta1==0.14.0
six==1.10.0
requests==2.12.4
google-auth==0.5.0
```

**Windows**
```
#!bash
$ pip install <requirements>
```

**Run**

```
#!bash
$ python transcribe_streaming.py
```


