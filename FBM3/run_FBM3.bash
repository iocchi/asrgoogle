#!/bin/bash
echo $1
TRFILE="transcriptions_`date +%Y%m%d_%H%M%S`.txt"
export GOOGLE_APPLICATION_CREDENTIALS=$HOME/src/Speech/asrgoogle/Speech-792a2486c29d.json
cd ../asrgoogle/transcribe 
python transcribe.py "$1" $TRFILE
python resultsFBM3.py $TRFILE "$1/SPQReL.txt"

