#!/usr/bin/env python
# encoding=utf8

# encoding=utf8
import argparse
import sys
import requests

# run server: java -jar -Xmx1G lu4r-server-0.2.1.jar simple cfr en 9001
lu4r_ip = '127.0.0.1'
lu4r_port = '9001'
lu4r_url = 'http://' + lu4r_ip + ':' + str(lu4r_port) + '/service/nlu'

parser = argparse.ArgumentParser()
parser.add_argument('input_file', help='Full path of folder with audio files to be recognized')
parser.add_argument('output_file', help='Output file with transcriptions')
args = parser.parse_args()
sys.stdout = open(args.output_file,'w')

HEADERS = {'content-type': 'application/json'}
f=open(args.input_file,'r')
for line in f:
	if line[0] == '#':
		continue
	line=line.strip()
	if line=="":
		continue
	line=line.split('|',1)
	testo=line[0]
	line=line[1]
	if line != "":
		partdiv=line.split(':')
		#print partdiv
		i=0
		for element in partdiv:
			#print "Text: ",element
			i+=1	
			data= '{\"hypotheses\":[{\"transcription\":\" ' + element  + '\", \"confidence\":0.9,\"rank\":0}]}'
			entities='{\"entities\":[]}'
			to_send = {'hypo': data, 'entities': entities}
			response = requests.post(lu4r_url, to_send, headers=HEADERS)
			#print "LU4R response: ",response.text

			answer=response.text.strip()
			#print answer
			if answer != 'NO FRAME(S) FOUND':
				#stringdiv=answer.split('(',1)
				#if stringdiv[0] == 'LOCATING' and len(stringdiv)>0:
				#	print testo + '|' + element + '|' + 'SEARCHING(' + stringdiv[1]
				#else:
				if "MOTION)" in answer:
					answer = answer.replace("MOTION)","MOTION()")
				if "ING)" in answer:
					answer = answer.replace("ING)","ING()")
				print testo + '|' + element + '|' + answer
				break
			if answer == 'NO FRAME(S) FOUND' and len(partdiv)==i:
				print testo + '|' + element + '|' + 'NO_INTERPRETATION'




