#!/usr/bin/env python
# Copyright 2016 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Google Cloud Speech API sample application using the REST API for batch
processing."""

# [START import_libraries]
import argparse
import base64
import json
import os

from googleapiclient import discovery
import httplib2
from oauth2client.client import GoogleCredentials, ApplicationDefaultCredentialsError
# [END import_libraries]

COL_WARNING = '\033[93m'
COL_FAIL = '\033[91m'
COL_RESET = '\033[0m'

def printerr(s):
    print "%s%s%s" % (COL_FAIL,s,COL_RESET)

def printwarn(s):
    print "%s%s%s" % (COL_WARNING,s,COL_RESET)

# [START authenticating]
DISCOVERY_URL = ('https://{api}.googleapis.com/$discovery/rest?'
                 'version={apiVersion}')


# Application default credentials provided by env variable
# GOOGLE_APPLICATION_CREDENTIALS
def get_speech_service():
    credentials = GoogleCredentials.get_application_default().create_scoped(
        ['https://www.googleapis.com/auth/cloud-platform'])
    http = httplib2.Http()
    credentials.authorize(http)

    return discovery.build(
        'speech', 'v1beta1', http=http, discoveryServiceUrl=DISCOVERY_URL)
# [END authenticating]

# global variable for Google service
service=None

def transcribe(speech_file):
    global service
    """Transcribe the given audio file.

    Args:
        speech_file: the name of the audio file.
    """
    # [START construct_request]
    with open(speech_file, 'rb') as speech:
        # Base64 encode the binary audio file for inclusion in the JSON
        # request.
        speech_content = base64.b64encode(speech.read())

    if (service==None):
        print "Connecting to Google services ..."
        try:
            service = get_speech_service()
        except httplib2.ServerNotFoundError:
            printerr('Cannot connect to Google service!!!')
            return ''
        except ApplicationDefaultCredentialsError:
            printwarn('No auth file. Using cache!!!')
            return transcribe_cache(speech_file)
        except:
            printerr('Unexpected error!!!')
            return ''

    service_request = service.speech().syncrecognize(
        body={
            'config': {
                # There are a bunch of config options you can specify. See
                # https://goo.gl/KPZn97 for the full list.
                #'encoding': 'LINEAR16',  # raw 16-bit signed LE samples
                'encoding': 'FLAC',  # raw 16-bit signed LE samples
                'sampleRate': 44100,  # 16 khz
                # See http://g.co/cloud/speech/docs/languages for a list of
                # supported languages.
                'languageCode': 'en-US',  # a BCP-47 language tag
                'maxAlternatives': 3,
            },
            'audio': {
                'content': speech_content.decode('UTF-8')
                }
            })
    # [END construct_request]
    # [START send_request]
    print "Executing Google request ..."
    try:
        response = service_request.execute()
    except:
        printerr("Cannot execute Google request!!!")
        return ''
    return json.dumps(response)
    # [END send_request]



def transcribe_cache(speech_file):
    if (speech_file.endswith("e1.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.41437808, "transcript": "go to Studio"}, {"transcript": "go to the studio"}, {"transcript": "Open studio"}]}]}'
    if (speech_file.endswith("e2.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.54095113, "transcript": "please move to the fridge"}, {"transcript": "nice move to the fridge"}, {"transcript": "The Smurfs 2 the fridge"}]}]}'
    if (speech_file.endswith("e3.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.8989623, "transcript": "put the bottle in the bin"}, {"transcript": "put the bottle in the bed"}, {"transcript": "put the bottle in the bag"}]}]}'
    if (speech_file.endswith("e4.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.982679, "transcript": "go to the laundry room"}]}]}'
    if (speech_file.endswith("e5.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.91629094, "transcript": "I need some utensils can you get me some"}, {"transcript": "I need some intensive can you get me some"}, {"transcript": "I need some attention can you get me some"}]}]}'
    if (speech_file.endswith("e6.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.4896408, "transcript": "Bluetooth bedroom"}, {"transcript": "food for the bedroom"}, {"transcript": "move towards the bedroom"}]}]}'
    if (speech_file.endswith("e7.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.93796045, "transcript": "go to the coffee machine"}, {"transcript": "go to the copy machine"}, {"transcript": "go to the confirmation"}]}]}'
    if (speech_file.endswith("e8.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.8001771, "transcript": "please take my phone into the living room"}, {"transcript": "please take my phone in the living room"}, {"transcript": "she take my phone into the living room"}]}]}'
    if (speech_file.endswith("e9.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.97265124, "transcript": "please go to the kitchen through the door"}, {"transcript": "please go to the kitchen to the door"}]}]}'
    if (speech_file.endswith("e10.flac")):
        return '{"results": [{"alternatives": [{"confidence": 0.7399964, "transcript": "did you hang my clothes on the call you so please"}, {"transcript": "did you hang my clothes on the console please"}, {"transcript": "did you hang my clothes on the Play Store please"}]}]}'
    return ''


def transcribe_folder(folder, output_file):
    print "Looking for wav files in",folder
    print "Writing results to file",output_file
    of = open(output_file, 'w')
    for f in sorted(os.listdir(folder)):
        if f.endswith(".wav"):
            print(f)
            wavf = folder+"/"+f
            flacf = folder+"/"+f[:-4]+".flac"
            if not os.path.isfile(flacf):
                #cmd = 'ffmpeg -i "%s" -map_channel 0.0.0 "%s" -y -bits_per_raw_sample 16 -ar 44100 ' % (wavf, flacf)
                cmd = 'ffmpeg -i "%s" -c:a flac -ac 1  -y -bits_per_raw_sample 16 -ar 44100 "%s"' % (wavf, flacf)
                print cmd
                os.system(cmd)
            #r = transcribe_cache(flacf)
            r = transcribe(flacf)
            if (r!=''):
                print f
                print r
                j = json.loads(r)
                aa = j['results'][0]['alternatives']
                conf = aa[0]['confidence']
                print "Confidence:", conf
                of.write("# %s Confidence: %.3f\n" % (f,conf))
                #of.write('%s|%s|NO_INTERPRETATION\n' % (f,aa[0]['transcript']))
                of.write('%s|' % (f))
                k = len(aa)
                for i in range(0,k):
                    ti = aa[i]['transcript']
                    of.write('%s' % (ti))
                    if (i<k-1):
                        of.write(':')
                of.write('\n')
            else:
                of.write('%s|BAD_RECOGNITION\n' % (f))

    of.close()


# [START run_application]
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('speech_file_folder', help='Full path of folder with audio files to be recognized')
    parser.add_argument('output_file', help='Output file with transcriptions')
    args = parser.parse_args()
    transcribe_folder(args.speech_file_folder, args.output_file)
# [END run_application]

